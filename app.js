var app = angular.module("base", ["ngRoute"]);

app.config(["$routeProvider", function($routeProvider) {
    
    if (route.length > 0) {
        angular.forEach(route, function(mRoute, index) {
            $routeProvider
                .when(mRoute.path, {
                    templateUrl: 'views/' + mRoute.template,
                    controller: mRoute.controller,
                    resolve: function() {
                        console.log("resolving", mRoute.path);
                    }
                })
        })
    }
    $routeProvider.otherwise("/dashboard");
}]);

app.factory('Storage', Storage);

app.factory('APIService', APIService);

app.controller("homeCtrlr",['$scope','APIService', function($scope, APIService) {

   /* $rootScope.$on('$routeChangeStart', function(next, last) {
        console.log("location change started");
    });*/
    console.log("Home ctrlr loaded");
    $scope.menus = [];
               $scope.showTopMenu=false;
APIService.getMenus(function(data){
        console.log("Rcvd menus",data);
               $scope.showTopMenu=true;

    $scope.menus=data;
    $scope.showSubMenu=false;
    $scope.toggleSubmenu=function(){
        $scope.showSubMenu=!$scope.showSubMenu;
            console.log("toggle",$scope.showSubMenu);

    }
});
}]);

app.controller("dashboardController", ["$scope", "APIService", function($scope, APIService) {
    console.log("dashboard loaded");
}]);


this.load = function(ctrlrName) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.charset = 'utf-8';
    script.async = true;
    script.onload = function() {
        console.log('The script is loaded');
    }
    script.src = "/controllers/" + ctrlrName.replace("Ctrlr", "") + ".js";
    head.appendChild(script);
}
