var WEBSERVICE={};
WEBSERVICE.HOSTNAME="http://54.169.235.136";
WEBSERVICE.ROUTE={};
WEBSERVICE.ROUTE.menus="/appMenus"
WEBSERVICE.ROUTE.dashboard="/dashboard"
WEBSERVICE.URI={};
WEBSERVICE.URI.getURI=function(endpoint,param){
    if(WEBSERVICE.ROUTE[endpoint]){
            return WEBSERVICE.HOSTNAME+WEBSERVICE.ROUTE[endpoint].replace(":param",param);
    }else{
        return WEBSERVICE.HOSTNAME;
    }
}


var route=[{
  controller:'dashboardController',
  path:'/dashboard',
  template:'dashboard.html',
  src:'dashboard.js'
}]
