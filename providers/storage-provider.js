function Storage(){
    var _s=localStorage;
    var _const={}
    _const.menu="_menus";
    _const.user_info="user_info";
    
    this.save=function(name,value){
        return _s.setItem(name,value);
    }
    this.get=function(name,isJson){
        var _tmp= _s.getItem(name);
        if(isJson&&_tmp){
            return JSON.parse(_tmp);
        }else{
            return _tmp;
        }
    }
    return {
        key:_const,
        get:this.get
    }
}