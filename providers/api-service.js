var APIService = function (Storage, $http) {
    this.getMenus = function (cb) {
        var _menus = [];
        var _now = Math.floor(Date.now() / 1000);
        var _tmp = Storage.get(Storage.key.menus,true);
           
        if (_tmp) {
           if (_tmp.expiry > _now) {
                cb(_tmp.menus);
                return;
            }
        }

        $http.get(WEBSERVICE.URI.getURI('menus')).then(function (res) {
            if (res.status == 200) {
                cb(res.data.data[0].data);
            }
            else {
                cb([]);
            }
        }, function () {
            cb([]);
        })
    }
    this.get = function (url,cb) {
        return $http.get(url).then(function (data) {
          cb(data);
        })
    }
    return {
        get: this.get,
        getMenus:this.getMenus
    }
}